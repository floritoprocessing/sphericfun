import peasy.*;

int detail = 40;

SphericObject sphere0;
SphericObject sphere1;
SphericObject sphere2;
SphericObject sphere3;
//SphericObject morph;

PeasyCam cam;

boolean showOriginals = false;
boolean autoMorph = true;

int lastMillis;
int morphTime = 0;

float morphFactorX = 0.5;
float morphFactorY = 0.5;
//float brownyDir;
//float brownySpeed = 0.01;
//float brownyDirChange = 0.1;

void setup() {
  size(800, 600, P3D);
  cam = new PeasyCam(this, 1000);
  
  println("[space] to toggle view  mode");
  println("[m] to toggle auto morph");
  
  //brownyDir = random(TWO_PI);

  sphere0 = new SphericObject(
    createConeFromSphere(buildSphere(200, detail, detail*2), 300, 50), 
    //buildSphere(200, detail, detail*2),
    buildLatStripes(detail, detail*2, color(50, 255, 20), color(0, 100, 5))
    //color(255, 40, 20)
    );//, buildLatStripes(15,30));

  sphere1 = new SphericObject(
    //createLatLonStar(buildSphere(200, detail, detail*2), 0.6, 1.0, 5, 0.7, 1.2, 5), 
    buildCube(200, detail, detail*2), 
    buildLatStripes(detail, detail*2, color(255, 50, 20), color(50, 20, 255))
    );

  sphere2 = new SphericObject(
    //createLatLonStar(buildSphere(200, detail, detail*2), 0.6, 1.0, 5, 0.7, 1.2, 5), 
    buildSphere(200, detail, detail*2), 
    buildDiagStripes(detail, detail*2, color(140, 50, 180), color(50, 20, 255))
    );

  sphere3 = new SphericObject(
    createLatLonStar(buildSphere(200, detail, detail*2), 0.6, 1.0, 5, 0.7, 1.2, 5), 
    buildLonStripes(detail, detail*2, color(10, 50, 255), color(80, 190, 20))
    );

  lastMillis = millis();
}

void keyPressed() {
  if (key==' ') {
    showOriginals = !showOriginals;
  } else if (key=='m') {
    autoMorph = !autoMorph;
  }
}

void draw() {
  int time = millis();
  int deltaMillis = time-lastMillis;

  background(240);

  smooth();
  ambientLight(128, 128, 128);
  directionalLight(128, 128, 128, 0.5, -0.5, -1);  
  lightFalloff(1, 0, 0);
  lightSpecular(0, 0, 0);

  stroke(0);
  strokeWeight(1);
  line(-50, 0, 0, 50, 0, 0);
  line(0, -50, 0, 0, 50, 0);
  line(0, 0, -50, 0, 0, 50);

  strokeWeight(1);
  noFill();


  noStroke();
  //fill(255, 50, 20);

  if (showOriginals) {

    pushMatrix();
    translate(-250, -250, 0);
    sphere0.draw();
    popMatrix();

    pushMatrix();
    translate(250, -250, 0);
    sphere1.draw();
    popMatrix();

    pushMatrix();
    translate(-250, 250, 0);
    sphere2.draw();
    popMatrix();

    pushMatrix();
    translate(250, 250, 0);
    sphere3.draw();
    popMatrix();
  } else {

    morphTime += deltaMillis;

    //float freq = 0.3;
    //float phase = map(morphTime, 0, 1000/freq, 0, TWO_PI);
    //float morphFactor = map(sin(phase), -1, 1, 0, 1);



    if (!autoMorph) {
      morphFactorX = map(mouseX, 0, width, 0, 1);
      morphFactorY = map(mouseY, 0, height, 0, 1);
    } else {
      
      //brownyDir += random(-brownyDirChange,brownyDirChange);
      //float mx = brownySpeed * cos(brownyDir);
      //float my = brownySpeed * sin(brownyDir);
      //morphFactorX += mx;
      //morphFactorX = max(0,min(1,morphFactorX));
      //morphFactorY += my;
      //morphFactorY = max(0,min(1,morphFactorY));
      
      // FIGURE 8;
      
      float phase = map(millis(),0,2000,0,TWO_PI); // figure 8 phase
      float rotPhase = map(millis(),0,20000,0,TWO_PI);
      
      float ampH = 0.3;
      float ampV = 0.9;
      float horiz = ampH*sin(phase*2);
      float vert = ampV*sin(phase);
      PVector eight = new PVector(horiz,vert);
      eight.rotate(rotPhase);
      
      morphFactorX = map(eight.x,-1,1,0,1);
      morphFactorY = map(eight.y,-1,1,0,1);
      
    }

    SphericObject morph01 = morphSphericObject( sphere0, sphere1, morphFactorX );
    SphericObject morph23 = morphSphericObject( sphere2, sphere3, morphFactorX );
    SphericObject morph = morphSphericObject( morph01, morph23, morphFactorY );

    pushMatrix();
    morph.draw();
    popMatrix();
  }



  lastMillis = time;
}

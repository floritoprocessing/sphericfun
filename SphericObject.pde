class SphericObject {



  /** 2-dimensional array of vertices sorted by latitude/longitude */
  PVector[][] vertices;
  int[][] fillColors;

  SphericObject(PVector[][] inVertices, int[][] inColors) {
    vertices = inVertices;
    fillColors = inColors;
  }

  SphericObject(PVector[][] inVertices, int inColor) {
    vertices = inVertices;

    int latDetail = vertices.length;
    int lonDetail = vertices[0].length;
    fillColors = new int[latDetail][lonDetail];
    for (int lat=0; lat<latDetail; lat++) {
      for (int lon=0; lon<lonDetail; lon++) {
        fillColors[lat][lon] = inColor;
      }
    }
  }



  /*
  *
   *  DRAW
   *
   */

  void draw() {


    // top & bottom:
    for (int lat=1; lat<vertices.length; lat+=vertices.length-3) {
      int lat0=lat==1 ? 0 : vertices.length-1;

      beginShape(TRIANGLE_FAN);
      PVector cen = vertices[lat0][0];

      fill( fillColors[lat0][0] );

      //fill(latColor[lat0]);
      vertex(cen.x, cen.y, cen.z);
      int lonCount = vertices[lat].length;
      for (int longitude=0; longitude<lonCount; longitude++) {
        int lon = longitude%lonCount;
        PVector a = vertices[lat][lon];
        fill( fillColors[lat][lon] );
        //fill(latColor[lat]);
        vertex(a.x, a.y, a.z);
      }
      endShape();
    }


    // others
    for (int lat=1; lat<vertices.length-2; lat++) {
      //fill(latColor[lat]);
      beginShape(TRIANGLE_STRIP);
      int lonCount = vertices[lat].length;
      for (int longitude=0; longitude<lonCount; longitude++) {
        int lon = longitude % lonCount;
        PVector a = vertices[lat][lon];
        PVector b = vertices[lat+1][lon];
        //fill(latColor[lat]);
        fill( fillColors[lat][lon] );
        vertex(a.x, a.y, a.z);
        //fill(latColor[lat+1]);
        fill( fillColors[lat+1][lon] );
        vertex(b.x, b.y, b.z);
      }
      endShape();
    }
  }
}






// @see https://en.wikipedia.org/wiki/Spherical_coordinate_system
PVector[][] buildSphere(float radius, int latDetail, int lonDetail) {  
  PVector[][] out = new PVector[latDetail][lonDetail];
  for (int lat=0; lat<latDetail; lat++) {
    float latitude = map(lat, 0, latDetail-1, 0, PI);
    for (int lon=0; lon<lonDetail; lon++) {
      float longitude = map(lon, 0, lonDetail-1, 0, TWO_PI);
      PVector point = fromAngle(longitude, latitude).mult(radius);
      out[lat][lon] = point;
    }
  }
  return out;
}


// @see https://en.wikipedia.org/wiki/Spherical_coordinate_system
PVector[][] buildCube(float radius, int latDetail, int lonDetail) {  
  PVector[][] out = new PVector[latDetail][lonDetail];
  for (int lat=0; lat<latDetail; lat++) {
    float latitude = map(lat, 0, latDetail-1, 0, PI);
    for (int lon=0; lon<lonDetail; lon++) {
      float longitude = map(lon, 0, lonDetail-1, 0, TWO_PI);
      
      PVector point = fromAngle(longitude, latitude).mult(radius);
      float hp3 = HALF_PI*3;
      if (longitude<HALF_PI) {
        point.x = map(longitude,0,HALF_PI,-radius,0);
        point.y = map(longitude,0,HALF_PI,0,-radius);
      } else if (longitude<PI) {
        point.x = map(longitude,HALF_PI,PI,0,radius);
        point.y = map(longitude,HALF_PI,PI,-radius,0);
      } else if (longitude<hp3) {
        point.x = map(longitude,PI,hp3,radius,0);
        point.y = map(longitude,PI,hp3,0,radius);
      } else {
        point.x = map(longitude,hp3,TWO_PI,0,-radius);
        point.y = map(longitude,hp3,TWO_PI,radius,0);
      }
      
      out[lat][lon] = point;
    }
  }
  return out;
}


PVector[][] createLonStar(PVector[][] vertices, float minPerc, float maxPerc, int freq) {
  int latDetail = vertices.length;
  int lonDetail = vertices[0].length;
  PVector[][] out = new PVector[latDetail][lonDetail];
  for (int lat=0; lat<latDetail; lat++) {
    for (int lon=0; lon<lonDetail; lon++) {
      PVector v = vertices[lat][lon];
      float ampPhase = map(lon, 0, lonDetail-1, 0, TWO_PI*freq);
      float ampFac = map(sin(ampPhase), -1, 1, minPerc, maxPerc);
      float amp = v.mag() * ampFac;
      v.setMag(amp);
      out[lat][lon] = v;
    }
  }
  return out;
}

PVector[][] createLatLonStar(PVector[][] vertices, 
  float latMinPerc, float latMaxPerc, int latFreq, 
  float lonMinPerc, float lonMaxPerc, int lonFreq) {

  int latDetail = vertices.length;
  int lonDetail = vertices[0].length;
  PVector[][] out = new PVector[latDetail][lonDetail];
  for (int lat=0; lat<latDetail; lat++) {
    float latAmpPhase = map(lat, 0, latDetail-1, 0, TWO_PI*latFreq);
    float latAmpFac = map(sin(latAmpPhase), -1, 1, latMinPerc, latMaxPerc);
    for (int lon=0; lon<lonDetail; lon++) {
      PVector v = vertices[lat][lon];
      float ampPhase = map(lon, 0, lonDetail-1, 0, TWO_PI*lonFreq);
      float ampFac = map(sin(ampPhase), -1, 1, lonMinPerc, lonMaxPerc);
      float amp = v.mag() * ampFac * latAmpFac;
      v.setMag(amp);
      out[lat][lon] = v;
    }
  }
  return out;
}


PVector[][] createConeFromSphere(PVector[][] vertices, float baseRadius, float topRadius) {
  int latDetail = vertices.length;
  int lonDetail = vertices[0].length;
  PVector[][] out = new PVector[latDetail][lonDetail];
  
  // establish bottom and top
  float minZ = Float.MAX_VALUE;
  float maxZ = Float.MIN_VALUE;
  for (int lat=0; lat<latDetail; lat++) {
    for (int lon=0; lon<lonDetail; lon++) {
      PVector v = vertices[lat][lon];
      minZ = min(v.z, minZ);
      maxZ = max(v.z, maxZ);
    }
  }
  
  for (int lat=0; lat<latDetail; lat++) {
    for (int lon=0; lon<lonDetail; lon++) {
      
      PVector v = vertices[lat][lon];
      PVector onCenterLine = new PVector(0,0,v.z);
      PVector lineFromCenterToSphere = PVector.sub( v, onCenterLine);
      float targetCircleRadius = map(v.z,minZ,maxZ,baseRadius,topRadius);
      lineFromCenterToSphere.setMag(targetCircleRadius);      
      out[lat][lon] = PVector.add( onCenterLine, lineFromCenterToSphere );
      
    }
  }
  
  
  return out;
}


int[][] buildLatStripes(int latDetail, int lonDetail, int color0, int color1) {
  int[][] out = new int[latDetail][lonDetail];
  for (int lat=0; lat<latDetail; lat++) {
    for (int lon=0; lon<lonDetail; lon++) {
      out[lat][lon] = lat%2==0 ? color0 : color1;
    }
  }
  return out;
}

int[][] buildLonStripes(int latDetail, int lonDetail, int color0, int color1) {
  int[][] out = new int[latDetail][lonDetail];
  for (int lat=0; lat<latDetail; lat++) {
    for (int lon=0; lon<lonDetail; lon++) {
      out[lat][lon] = lon%2==0 ? color0 : color1;
    }
  }
  return out;
}

int[][] buildDiagStripes(int latDetail, int lonDetail, int color0, int color1) {
  int[][] out = new int[latDetail][lonDetail];
  for (int lat=0; lat<latDetail; lat++) {
    for (int lon=0; lon<lonDetail; lon++) {
      out[lat][lon] = (lat+lon)%2==0 ? color0 : color1;
    }
  }
  return out;
}

PVector fromAngle(float theta, float phi) {
  float sinphi = sin(-phi);
  PVector out = new PVector();
  return out.set(cos(theta) * sinphi, 
    (sin(theta) * sinphi), 
    cos(-phi));
}




SphericObject morphSphericObject(SphericObject s0, SphericObject s1, float morphFactor) {

  if (s0.vertices.length != s1.vertices.length ||
    s0.vertices[0].length != s1.vertices[0].length ||
    (s0.fillColors==null && s1.fillColors!=null) ||
    (s0.fillColors!=null && s1.fillColors==null)) {
    throw new RuntimeException("Incompatible SphericObjects");
  } else {

    int latDetail = s0.vertices.length;
    int lonDetail = s0.vertices[0].length;
    PVector[][] vertices = new PVector[latDetail][lonDetail];
    for (int lat=0; lat<latDetail; lat++) {
      for (int lon=0; lon<lonDetail; lon++) {
        PVector v0 = s0.vertices[lat][lon];
        PVector v1 = s1.vertices[lat][lon];
        vertices[lat][lon] = new PVector(
          v0.x + morphFactor*(v1.x-v0.x), 
          v0.y + morphFactor*(v1.y-v0.y), 
          v0.z + morphFactor*(v1.z-v0.z)
          );
      }
    }

    int[][] fillColors = null;
    
    // interpolate vertex colors
    fillColors = new int[latDetail][lonDetail];
    for (int lat=0; lat<latDetail; lat++) {
      for (int lon=0; lon<lonDetail; lon++) {
        fillColors[lat][lon] = interpolateColor(s0.fillColors[lat][lon], s1.fillColors[lat][lon], morphFactor);
      }
    }


    SphericObject out = new SphericObject(vertices, fillColors);
    return out;
  }
}


int interpolateColor(int c0, int c1, float f1) {
  float f0 = 1-f1;
  int a = max(0, min(255, (int)((c0>>24&0xff)*f0 + (c1>>24&0xff)*f1)));
  int r = max(0, min(255, (int)((c0>>16&0xff)*f0 + (c1>>16&0xff)*f1)));
  int g = max(0, min(255, (int)((c0>> 8&0xff)*f0 + (c1>> 8&0xff)*f1)));
  int b = max(0, min(255, (int)((c0&0xff)*f0 + (c1&0xff)*f1)));
  return a<<24 | r<<16 | g<<8 | b;
}
